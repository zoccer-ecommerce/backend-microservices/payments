# Zoccer Ecommerce Payments
Payments microservice for Zoccer Ecommerce platform
## Description
This project contains several payments APIs for Zoccer Ecommerce platform. The payment services currently provided are:
- PayPal
## Getting Started
### Dependencies
Before installing the program, make sure you have the following in your system:
* [Nodejs](https://nodejs.org/en/download/) (version 14.x preferred)
* [Yarn](https://classic.yarnpkg.com/en/docs/install/#windows-stable)
### Installing
* Clone the [project](https://gitlab.com/zoccer-ecommerce/backend-microservices/payments.git) to a desired location on your system
* Run `yarn install` command from the terminal (at the root directory of the project) to install the project dependencies
* Add `.env.development` file in the root directory of the project, and add environment variables as shown below (replace the values in square beackets with your own):
```
  NODE_ENV=development
  PORT=[YOUR_SERVER_PORT]
  PAYPAL_ENV=[YOUR_PAYPAL_ENVIRONMENT]
  PAYPAL_CLIENT_ID=[YOUR_PAYPAL_CLIENT_ID]
  PAYPAL_CLIENT_SECRET=[YOUR_PAYPAL_CLIENT_SECRET]
```
### Executing program
Run the command below from the root directory of the project
```
yarn start:dev
```
<!-- ## Help
Any advise for common problems or issues.
```
command to run if program contains helper info
``` -->
## Authors
<!-- Contributors names and contact info --> 
- Moses Adongo ([Gitlab repo](https://gitlab.com/AdongoJr))
<!-- ## Version History
* 0.2
* Various bug fixes and optimizations
* See [commit change]() or See [release history]()
* 0.1
* Initial Release -->
## License
This project is licensed under the `ISC` License 
<!-- see the LICENSE.md file for details -->
<!-- ## Acknowledgments
Inspiration, code snippets, etc.
* [awesome-readme](https://github.com/matiassingers/awesome-readme)
* [PurpleBooth](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
* [dbader](https://github.com/dbader/readme-template)
* [zenorocha](https://gist.github.com/zenorocha/4526327)
* [fvcproductions](https://gist.github.com/fvcproductions/1bfc2d4aecb01a834b46) -->