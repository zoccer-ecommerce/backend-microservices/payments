import checkoutNodeJssdk from '@paypal/checkout-server-sdk';


/**
 *
 * Set up and return PayPal JavaScript SDK environment with PayPal access credentials.
 * This sample uses SandboxEnvironment. In production, use LiveEnvironment.
 *
 */
function environment() {
  let clientId: string = process.env.PAYPAL_CLIENT_ID as string;
  let clientSecret: string = process.env.PAYPAL_CLIENT_SECRET as string;
  const paypalEnv: string = process.env.PAYPAL_ENV as string;

  if (paypalEnv === "sandbox") {  // sandbox environment
    return new checkoutNodeJssdk.core.SandboxEnvironment(
      clientId, clientSecret
    );
  } else if (paypalEnv === "production") { // production environment
    return new checkoutNodeJssdk.core.ProductionEnvironment(
      clientId, clientSecret
    );
  }
}

/**
  *
  * Returns PayPal HTTP client instance with environment that has access
  * credentials context. Use this instance to invoke PayPal APIs, provided the
  * credentials have access.
  */
export function client() {
  return new checkoutNodeJssdk.core.PayPalHttpClient(environment());
}

