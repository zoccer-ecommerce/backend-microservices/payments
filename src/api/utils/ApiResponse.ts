import { Response } from "express";

class ApiResponse {
  statusCode: any;
  message: any;
  type: any;
  data: any;

  constructor() {
    this.statusCode = null;
    this.message = null;
    this.data = null;
    this.type = null;
  }

  setSuccess(statusCode: number, message: string, data?: any): void {
    // TODO: allow only 2** status codes
    this.statusCode = statusCode;
    this.message = message;
    this.data = data;
    this.type = "success";
  }

  setError(statusCode: number, message: string, data?: any): void {
    // TODO: do not allow 2** status codes
    this.statusCode = statusCode;
    this.message = message;
    this.data = data;
    this.type = "error";
  }

  send(res: Response) {
    const result = this.data
      ? {
        status: this.type,
        message: this.message,
        data: this.data,
      } : {
        status: this.type,
        message: this.message,
      }

    return res.status(this.statusCode).json(result);
  }

  sendError(err: any, res: Response) {
    console.log();
    console.log(err);

    // checking whether the error is custom (thrown by us)
    if (err.isCustom) {
      this.setError(err.status, err.message);
      return this.send(res);
    }

    // unknown errors
    this.setError(500, "Some error occurred");
    return this.send(res);
  }
  
}

export default ApiResponse;
