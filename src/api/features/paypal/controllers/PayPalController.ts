import { Request, Response } from "express";
import PaypalService from "../services/PayPalService";
import ApiResponse from "../../../utils/ApiResponse";

const response = new ApiResponse();

class PayPalController {
  // PAYPAL
  static async createOrdersRequest(req: Request, res: Response) {
    const { amount } = req.body;
    
    // validations TODO: move validations to middleware
    if (!amount) {
      response.setError(400, "amount field is required");
      return response.send(res);
    }
    if (!Number(amount)) {
      response.setError(400, "amount must be a valid numerical string");
      return response.send(res);
    }

    try {
      const { orderID: orderId } = await PaypalService.createOrdersRequest({ value: amount });

      response.setSuccess(200, "Order Request success", { orderId });
      return response.send(res);
    }
    catch (err) {
      response.sendError(err, res);
    }
  }

  static async captureTransaction(req: Request, res: Response) {
    const { orderId } = req.body;

    if (!orderId) {
      response.setError(400, "orderId field is required");
      return response.send(res);
    }

    try {
      const result = await PaypalService.captureTransaction(orderId);
      // TODO: save the capture ID to the database for future reference.
      const captureID = result.purchase_units[0].payments.captures[0].id
      console.log();
      console.log("Capture ID ->", captureID);
      console.log();

      response.setSuccess(200, "Order captured successfully", result);
      return response.send(res);
    }
    catch (err) {
      // TODO: move this logic to PayPalService
      interface PayPalErrorDetails {
        issue: string;
        description: string;
      }

      console.log();
      console.log("Paypal Error ->", err);
      console.log();

      const error: Error = err
      const errorDetails: PayPalErrorDetails[] = JSON.parse(error.message).details

      // console.log();
      // console.log("Paypal Error Details ->", errorDetails[0]);
      // console.log();

      // TODO: derive better/readable error status and messages
      response.setError(500, "Order could not be captured", errorDetails[0]);
      return response.send(res);
    }
  }
}

export default PayPalController;