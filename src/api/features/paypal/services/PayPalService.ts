import checkoutNodeJssdk from '@paypal/checkout-server-sdk';
import { client as payPalClient } from "../../../../config/payments/paypal";

class PaypalService {
  /**
   * 
   * @param payload value should contain a valid numerical value
   * @returns orderID if resolved if Promise is resolved
   */
  static async createOrdersRequest(payload: { value: string }) {
    try {
      // call PayPal to set up a transaction
      const request = new checkoutNodeJssdk.orders.OrdersCreateRequest();
      request.prefer("return=representation");
      request.requestBody({
        intent: 'CAPTURE',
        purchase_units: [{
          amount: {
            currency_code: 'USD',
            value: payload.value
          }
        }]
      });

      const order = await payPalClient().execute(request);

      console.log()
      console.log("Order Result ->", order.result)
      console.log()

      return {
        orderID: order.result.id,
      };

    }
    catch (error) {
      throw error;
    }
  }

  static async captureTransaction(orderID: any) {
    try {
      // Call PayPal to capture the order
      const request = new checkoutNodeJssdk.orders.OrdersCaptureRequest(orderID);
      request.requestBody({});

      const capture = await payPalClient().execute(request);

      console.log();
      console.log("Capture Result ->", capture.result);
      console.log();

      return capture.result;
    }
    catch (error) {
      throw error;
    }
  }
}

export default PaypalService;