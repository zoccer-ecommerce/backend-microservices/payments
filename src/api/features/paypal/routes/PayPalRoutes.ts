import { Router } from "express";
import multer from "multer";
const upload = multer();

import PayPalController from "../controllers/PayPalController";

const router = Router();

// PAYPAL
// TODO: add the auth middleware
router.post("/orders-create-request", [upload.none()], PayPalController.createOrdersRequest);
router.post("/orders-capture-request", [upload.none()], PayPalController.captureTransaction);

export default router;
