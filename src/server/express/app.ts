import express, { Request, Response } from "express";
const app = express();

require("custom-env").env();
import cors from "cors";

// routes import
import paymentRoutes from "../../api/features/paypal/routes/PayPalRoutes";

export const MODE = process.env.NODE_ENV;
export const PORT = process.env.PORT || 4000;
export const urlPrefix: string = "/api/v1" // api url prefix for almost all routes

app.use(cors({
  origin: "*"
})); // cors requests configuration
app.use(express.json()); // to parse JSON bodies

// welcome routes 
app.get("/", (req: Request, res: Response) => {
  res.json({ message: "Wecome to Zoccer Payments API" });
});
app.get(`${urlPrefix}/`, (req: Request, res: Response) => {
  res.json({ message: "Wecome to Zoccer Payments v1 APIs" });
});

// additional routes
app.use(`${urlPrefix}/payments`, paymentRoutes); // payments
// when a random route is inputed
app.get("*", (req: Request, res: Response) => {
  res.status(404).send({
    message: "No content available here.",
  })
});

export default app;