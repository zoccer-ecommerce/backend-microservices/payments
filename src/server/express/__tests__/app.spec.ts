import { urlPrefix } from "../app";

// sample test
describe("Sample App Test", () => {  
  it('should print correct api url prefix', () => {
    expect(urlPrefix).toBe("/api/v1");
  });
});